//
//  AppDelegate.h
//  CamStream
//
//  Created by Dimitar Dimitrov on 3/4/16.
//  Copyright © 2016 Dimitar Dimitrov. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <AVFoundation/AVFoundation.h>

@interface AppDelegate : AVCaptureOutput <NSApplicationDelegate, AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate>


@end

