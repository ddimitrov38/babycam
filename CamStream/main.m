//
//  main.m
//  CamStream
//
//  Created by Dimitar Dimitrov on 3/4/16.
//  Copyright © 2016 Dimitar Dimitrov. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
	return NSApplicationMain(argc, argv);
}
