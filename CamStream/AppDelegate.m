//
//  AppDelegate.m
//  CamStream
//
//  Created by Dimitar Dimitrov on 3/4/16.
//  Copyright © 2016 Dimitar Dimitrov. All rights reserved.
//

#import "AppDelegate.h"
#import "VideoEncoder.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

AVCaptureSession *captureSession;
AVCaptureDevice *camera;
//AVCaptureDevice *microphone;
AVCaptureVideoDataOutput *outputVideo;
//AVCaptureAudioDataOutput *outputAudio;

VideoEncoder *videoEncoder;

NSString *streamPath;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	NSError *error = nil;
	dispatch_queue_t captureQueue = dispatch_queue_create("com.dtd.cameraStream.CaptureQueue", nil);

	captureSession = [[AVCaptureSession alloc] init];
	camera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
//	microphone = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];

	outputVideo = [[AVCaptureVideoDataOutput alloc] init];
	[outputVideo setSampleBufferDelegate:self queue:captureQueue];

	// Audio
//	outputAudio = [[AVCaptureAudioDataOutput alloc] init];
//	[outputAudio setSampleBufferDelegate:self queue:captureQueue];

	AVCaptureDeviceInput *videoInput = [AVCaptureDeviceInput deviceInputWithDevice:camera error:&error];
//	AVCaptureDeviceInput *audioInout = [AVCaptureDeviceInput deviceInputWithDevice:microphone error:&error];

	[captureSession addInput:videoInput];
//	[captureSession addInput:audioInout];
	[captureSession addOutput:outputVideo];

	// Setup encoder
	int videoWidth = 640;//(int)[[outputVideo.videoSettings objectForKey:@"Width"] integerValue];
	int videoHeight = 480;//(int)[[outputVideo.videoSettings objectForKey:@"Height"] integerValue];

	NSLog(@"Init %dx%d", videoWidth, videoHeight);

	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	streamPath = @"/Library/WebServer/Documents/stream.jpg"; //[NSString stringWithFormat:@"%@/test.jpg", documentsDirectory];
	NSLog(@"Streaming to %@", streamPath);
	
//	videoEncoder = [VideoEncoder encoderForPath:streamPath Height:videoHeight width:videoWidth channels:1 samples:32000.0f];

	// Start capturing
	[captureSession startRunning];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
	CMFormatDescriptionRef fmt = CMSampleBufferGetFormatDescription(sampleBuffer);
	const AudioStreamBasicDescription *asbd = CMAudioFormatDescriptionGetStreamBasicDescription(fmt);
	if (asbd)
		NSLog(@"Sample rate: %.2f, Channels: %d", asbd->mSampleRate, asbd->mChannelsPerFrame);

//	[videoEncoder encodeFrame:sampleBuffer isVideo:YES];

	CVImageBufferRef frame = CMSampleBufferGetImageBuffer(sampleBuffer);
	CVBufferRetain(frame);
	@synchronized (self) {
		CIImage* ciImage = [CIImage imageWithCVImageBuffer: frame];
		NSBitmapImageRep* bitmapRep =
		[[NSBitmapImageRep alloc] initWithCIImage: ciImage];
		
		NSDictionary *imageOptions = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:0.1f] forKey:NSImageCompressionFactor];
		NSData* jpgData =[bitmapRep representationUsingType:NSJPEGFileType properties: imageOptions];
		[jpgData writeToFile: streamPath atomically: NO];
	}
	CVBufferRelease(frame);

}

@end
